package com.nespresso.exercise.pisa;

/**
 * The Floor class holds the individual floor information
 * and acts as the observer class
 */
public class Floor {
		
	public int floorSize;	
	public String floorDesign;
	private int noOfWindows;
	
	public Floor(int floorSize, int noOfWindows) {		
		this.floorSize = floorSize;
		
		if(noOfWindows >= floorSize - 1) {
			this.noOfWindows = floorSize - 2;
		} else {
			this.noOfWindows = noOfWindows;
		}
		
		this.floorDesign = "";
	}
	
	public void update(int floorIndex, int noOfFloors) {
		setFloorDesign(floorIndex, noOfFloors);
	}
	
	public void setFloorDesign(int floorIndex, int noOfFloors) {
		floorDesign = new String(new char[floorSize]).replace("\0", "X");
		
		if(noOfWindows > 0) {			
			AddWindows(floorIndex, noOfFloors);
		}	
	}
	
	private void AddWindows(int floorIndex, int noOfFloors) {
		int startIndexOfWindows = (int)(Math.ceil((floorSize - (noOfWindows - 1)) / 2.0)) - 1;
		floorDesign = floorDesign.substring(0, startIndexOfWindows) + 
				new String(new char[noOfWindows]).replace("\0", "0") + 
				floorDesign.substring(startIndexOfWindows + noOfWindows, 
						floorSize);
		
		char[] floorDesignAsCharArray = floorDesign.toCharArray();
		
		if(floorIndex == noOfFloors - 2) {
			//space window groups larger than 3
			if(noOfWindows > 3) {
				floorDesign = groupWindows(floorDesignAsCharArray, 3);
			}
		} else if(floorIndex == noOfFloors - 3) {
			//space window groups larger than 2
			if(noOfWindows > 2) {
				floorDesign = groupWindows(floorDesignAsCharArray, 2);
			}
		} else if(floorIndex <= noOfFloors - 4) {
			//single windows
			if(noOfWindows > 1) {
				floorDesign = groupWindows(floorDesignAsCharArray, 1);
			}
		}
	}
	
	private String groupWindows(char[] floor, int g) {		
        int len = floor.length;

        //No. of 0s
        int nO = 0;

        //Current position
        int pos = 0;        

        //Do nothing for Xs in beginning
        while (floor[pos] == 'X') {
            pos++;
        }

        //Calculate No. of 0s
        int i = pos;
        while (i < len) {
            if (floor[i] == '0') {
                nO++;
            }
            i++;
        }

        while (pos < len - 1) {
            for (int j = 1; j <= g; j++) {
                if (nO > 0) {
                    floor[pos] = '0';
                    pos++;
                    nO--;
                }
            }
            floor[pos] = 'X';
            pos++;
        }
        
        //last element always X
        floor[len - 1] = 'X';
        
        String str = "";
        for (char s : floor) {
            str = str + s;
        }
        return str;
	}
}
