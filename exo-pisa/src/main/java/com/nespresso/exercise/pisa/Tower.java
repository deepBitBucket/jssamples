package com.nespresso.exercise.pisa;

import java.util.List;
import java.util.ArrayList;

/**
 * The Tower class holds the floors information and provides the 
 * functionality to add a new floor. On adding a new floor,
 * all the base floors are notified using observer pattern.
 */
public class Tower {

	public List<Floor> floors = new ArrayList<Floor>();
	
    public void addFloor(int floorSize) {
    	addFloorWithOrWithoutWindows(floorSize, 0);
    }

    public String printFloor(int floorIndex) {
    	if(!floors.isEmpty()) {
	    	Floor floor = floors.get(floorIndex);
	    	floor.setFloorDesign(floorIndex, floors.size());
	    	return floor.floorDesign;
    	}
    	return "";
    }

    public void addFloorWithWindows(int floorSize, int desiredNumberOfWindows) {
    	addFloorWithOrWithoutWindows(floorSize, desiredNumberOfWindows);
    }
    
    private void addFloorWithOrWithoutWindows(int floorSize, int desiredNumberOfWindows) {
    	if(!floors.isEmpty()){
    		Floor previousFloor = floors.get(floors.size() - 1);
    		if(previousFloor.floorSize >= floorSize) {
		    	Floor floor = new Floor(floorSize, desiredNumberOfWindows);
		    	floors.add(floor);
    		} else {
    			throw new IllegalArgumentException();
    		}
    	} else {
    		Floor floor = new Floor(floorSize, desiredNumberOfWindows);
	    	floors.add(floor);
    	}
    	notifyFloors();
    }
    
    private void notifyFloors() {
    	if(!floors.isEmpty()) {
	    	for(Floor floor : floors){
	    		floor.update(floors.indexOf(floor), floors.size());
	    	}
    	}
    }

}
