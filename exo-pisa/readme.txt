
In a renaissance tower you can group windows as long as the weight from the floors above allows it, so, for instance, in many towers and bell towers of the period you have the topmost floors with windows grouped by three or more and then the floors below with ever smaller groups of windows :

see attached San_Donato_Genova_01.jpg

The goal is building such a tower and respecting this rule.

Carlo
