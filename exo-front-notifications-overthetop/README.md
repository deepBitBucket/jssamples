# Notifications

The aim of this exercise is to build up a reusable notification module that will appear on the bottom right of the viewport.

Your code will expose an object in `window` providing one method with the following signature:

    Notifications.notify(msg[,options]);

where:

- `msg`: the message to be shown to the user (may be formatting HTML like bold, italics)
- `options` are the options passed to the plugins and can be:
    - `time`: amount of seconds to show the message (optional, defaults to 3 seconds)
    - `type`: a value in \['info', 'warn', 'error'\] (optional, defaults to 'info')
    - `icon`: an optional icon to appear on the left (give 20px space for the icon)

Target browser: any modern browser and IE 8 and up. Visual degradation for is acceptable (IE8).

Please document in the code the eventual degradations.

### Success criteria

The following results should be achieved.

### Look

A message is shown with the same look and feel than the screenshot using the defaults settings, as to be seen on this screenshot:

![Expected result](notifications.png "Expected result")

/!\ Note that the icon version is missing from the screenshot.

### Message types

The type of message is handled by setting the background color to signify the type (resp. white, yellow, red).

### Stackable

Message are stackable (most recent on top).

### Animation

The appearance and disappearance is animated (slide up/down).

### Manual removal from stack

It is possible to remove a notification "manually"

    var loading = Notifications.notify(
        'Loading...',
        1000  // set a very long time
        );

    // later
    loading.remove();    <-- this removes the "loading" notification.
