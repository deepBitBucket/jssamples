// your code here

var Notifications = {
    notify: function(message,options){
		if(message != null && typeof(message) != 'undefined'){
			var messagediv = document.createElement('div');
			$(messagediv).addClass('notification-box');
			if(options != null && typeof(options) != 'undefined')
			{
				if(options['type'] != null && typeof(options['type']) != 'undefined')
				{
					if(options['type'] == 'warn'){
						$(messagediv).addClass('notification-box-warning');
					}
					else if(options['type'] == 'error'){
						$(messagediv).addClass('notification-box-error');
					}
				}
				if(options['icon'] != null && typeof(options['icon']) != 'undefined')
				{
					var image = document.createElement('img');
					$(image).attr('src', options['icon']);
					$(image).addClass('icon-image');
					$(messagediv).append(image);
				}				
			}
			$(messagediv).append(message);			
			$(messagediv).hide().prependTo($('.notification-container')).slideDown();
			if(options != null && typeof(options) != 'undefined' && options['time'] != null && typeof(options['time']) != 'undefined')
			{
				setTimeout(function(){
				$(messagediv).slideUp('slow').remove();
				}, options['time'] * 1000);
			}
			return $(messagediv);
		}
	}
}